#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */



int main(int argc, char *argv[]) {
    
    double height,radius;
    double volume;
    const int pie=3.14285714286;
    
    printf("Enter the height of the cone: ");
    scanf("%lf",&height);
    printf("Enter the radius of the cone: ");
    scanf("%lf",&radius);
    volume=pie*(radius*radius)*height/3;  
    printf("Volume of the cone=%f",volume);  
    
    return 0;
}

