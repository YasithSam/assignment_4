#include <stdio.h>
#include <stdlib.h>

/* run this progrxm using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    
  int x,y;      

  printf("Enter the integers to swap:");
  scanf("%d %d",&x,&y);
  printf("Before swap x=%d y=%d",x,y);      

  x=x+y;//x=30    
  y=x-y;//y=10    
  x=x-y;//x=20    

  printf("\nAfter swap x=%d y=%d",x,y);    
    
  return 0;
}
