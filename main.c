#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    
     float n1, n2, n3;
     float sum, avg;

     
     printf("Enter three Numbers: ");
     scanf("%f %f %f",&n1, &n2, &n3);

     // calculate sum
     sum = n1 + n2 + n3;
     // calculate average
     avg = sum / 3;

     
    

     // display sum and average
     printf("Sum=%.2f\n", sum);
     printf("Average=%.2f\n",avg );

     return 0;
}
