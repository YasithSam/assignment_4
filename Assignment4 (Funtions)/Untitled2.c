/* Name- Yasith Samaradivakara,
   Std.Id- 202075,
   Logic Considered - Increase or decrease attendance for Rs.5 change by 20*/

#include <stdio.h>

//function to calculate the profit earn
int cal_profit(int price);
// calculating revenue he earned through selling
int cal_revenue(int price);
//Calculating attendance with respective to ticket price
int cal_attendance(int price);
// Calculate the expenditure owner had
int cal_expend(int price);
//Display the relationship table
void display();

int cal_attendance(int price){
    
    const int att = 120; 
   
    return att - ((price-15) /5*20);
    
}

int cal_revenue(int price){
    return price * cal_attendance(price);
     
}
int cal_expend(int price){
    const int x = 500;
    return x + 3 * cal_attendance(price); 
}

int cal_profit(int price){
   return cal_revenue(price)-cal_expend(price);

}
void display(){
	int i=5,profit;
	for (i=5;i<=50;i+=5){
		 profit= cal_profit(i);
		 printf("Rs.%d \t Rs.%d\n",i,profit);
	}
}


int main()
{
   	
	printf("Table displays the relationship between profit and ticket price\n");
	printf("Price \t Profit\n");
	display();
	printf("According to the table profit increases at a decreasing rate in the range of Rs.5 to Rs.25 of ticket price and after that it starts to decrease\nMaximum profit is Rs.1260 for the ticket price of Rs.25");
	
    return 0;
}

