#include <stdio.h>
#include <stdlib.h>

/* run this progrxm using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    
  float fahr, cel;
  printf("Enter the temperature in celsius: ");
  scanf("%f", &cel);

  fahr = (cel*9/5) + 32.0; //temperature conversion formula
  printf("\nTemperature in Fahrenheit: %.2f F\n", fahr);


  return 0;
}
